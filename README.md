# ABOUT

Repo for application to work with cryptocurrencies.

# COMMANDS

To run scheduling:
celery -A crypto beat --scheduler django_celery_beat.schedulers:DatabaseScheduler


To install all requirements run:
virtualenv --no-site-packages --distribute .env && source .env/bin/activate && pip install -r requirements.txt