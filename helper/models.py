from django.db import models


class InformationByCurrency(models.Model):
    name = models.CharField(max_length=200)
    changed_count = models.FloatField()
    changed_percent = models.FloatField()
    time_interval = models.IntegerField()
    current_price = models.FloatField()
    updated_date = models.DateTimeField(auto_now=True)
    # TODO Нету ограничения уникальности на name
