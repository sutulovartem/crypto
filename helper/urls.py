from django.conf.urls import url

from . import views

# TODO Сделать просмотр топа валют на текущий момент
urlpatterns = [
    url(
        r'^api/v1/crypto/(?P<interval>[0-9]+)/(?P<price_min>[0.0-9.0]+)/(?P<price_max>[0.0-9.0]+)$',
        views.get_last_information,
        name='get_last_information'
    ),
    url(
        r'^test$',
        views.test,
        name='test'
    )
]
