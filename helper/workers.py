import csv
import datetime

import pandas
import requests
import simplejson as json

from .dtos import Currency

FILENAME = "result.csv"

COLUMNS = ["name", "percent_changed", "percent_changed_more", "value"]


def run(parameters):
    currencyInfo = None
    try:
        dateStart = datetime.datetime.now().timestamp()
        currencyInfo = json.loads(
            requests.get("https://yobit.net/api/3/trades/" + parameters.currencyName).text).get(
            parameters.currencyName)
        # print("GETTED INFORMATION BY CURRENCY   " + parameters.currencyName + "  BY ___________ " + str(
        #     datetime.datetime.now().timestamp() - dateStart))
    except Exception:
        return
        # print("НЕ ДОСТАЕТСЯ ИНФОРМАЦИЯ (скорее всего нет торгов) ПО ВАЛЮТЕ --- " + parameters.currencyName)
    if currencyInfo:
        try:
            bestCurrency = getBest(currencyInfo=currencyInfo, name=parameters.currencyName,
                                   interval=parameters.interval,
                                   price_min=parameters.price_min, price_max=parameters.price_max)
        except Exception:
            return
            # print("CAN NOT ANALYZE CURRENCY --- " + parameters.currencyName)
        else:
            return bestCurrency


def getBest(currencyInfo, name, interval, price_min, price_max):
    dataFrame = pandas.DataFrame(currencyInfo)
    minValueInDataFrame = 0
    maxValueInDataFrame = 0
    if len(dataFrame.price) != 0:
        maxValueInDataFrame = max(dataFrame.price)
        minValueInDataFrame = min(dataFrame.price)
    filteredDataFrame = dataFrame[dataFrame.timestamp > datetime.datetime.now().timestamp() - int(interval)][
        dataFrame.type == "bid"][
        dataFrame.price >= float(price_min)][
        dataFrame.price <= float(price_max)]
    maxValueInFilteredDataFrame = float(0)
    minValueInFilteredDataFrame = float(0)
    boostPercentFromFilteredData = float(0)
    boostPercentFromAllData = float(0)
    if len(filteredDataFrame.price) != 0:
        maxValueInFilteredDataFrame = max(filteredDataFrame.price)
        minValueInFilteredDataFrame = min(filteredDataFrame.price)
        if maxValueInFilteredDataFrame and maxValueInDataFrame and minValueInFilteredDataFrame and minValueInDataFrame:
            boostPercentFromFilteredData = float(
                "{0:.1f}".format(maxValueInFilteredDataFrame * 100 / minValueInFilteredDataFrame))
            boostPercentFromAllData = float("{0:.1f}".format(maxValueInDataFrame * 100 / minValueInDataFrame))
    if not filteredDataFrame.empty:
        return Currency(name=name, percent_changed=boostPercentFromFilteredData, value=max(filteredDataFrame.price),
                        percent_changed_more=boostPercentFromAllData)
    else:
        return