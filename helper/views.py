import csv
import datetime
import os
from multiprocessing.dummy import Pool as ThreadPool

import pandas
import requests
import simplejson as json
from django.shortcuts import render_to_response
from redis import Redis
from django.core.cache import cache

from .dtos import Currency
from .dtos import ParametersDto
from .workers import run

NUM_WORKERS = 200

FILENAME = "result.csv"

ALL_CURRENCIES_INFO_URL = "https://yobit.net/api/3/info"

CURRENCIES_NAMES_KEY = "currencies"

COLUMNS = ["name", "percent_changed", "percent_changed_more", "value"]

redis = Redis()


def get_last_information(request, interval, price_min, price_max):
    date_start = datetime.datetime.now().timestamp()
    pool = ThreadPool(NUM_WORKERS)
    currencies_names = []
    if cache.get(CURRENCIES_NAMES_KEY) is None:
        currencies_names = get_filtered_by_name_currencies_from_server(url=ALL_CURRENCIES_INFO_URL,
                                                                       correct_names=["btc"])
        cache.set(CURRENCIES_NAMES_KEY, currencies_names)
    else:
        currencies_names = cache.get(CURRENCIES_NAMES_KEY)
    date_getted_currencies = datetime.datetime.now().timestamp()
    print("ЧТЕНИЕ ВАЛЮТ ___________________________ " + str(date_getted_currencies - date_start))
    parameters = transform_currencies_to_params(
        currencies_names=currencies_names,
        interval=interval, price_min=price_min,
        price_max=price_max)
    remove_old_results_file_if_exist()
    write_header()

    currencies = pool.map(run, parameters)
    print("ПОЛУЧЕНЫ И ПРОАНАЛИЗИРОВАНЫ ВАЛЮТЫ ЗА  ___________________________  " + str(
        datetime.datetime.now().timestamp() - date_getted_currencies))
    pool.close()
    pool.join()
    result = {
        'interval_time': interval,
        'price_min': price_min,
        'price_max': price_max,
        'date_built': datetime.datetime.now(),
        'currencies': filter_currencies_and_get_result(currencies=currencies)
    }
    return render_to_response('crypto.html', result)


def test(request):
    remove_old_results_file_if_exist()
    # currencies = loadCurrencies()
    result = {
        'interval_time': 60,
        'price_min': 1,
        'price_max': 2,
        'date_built': datetime.datetime.now(),
        # 'currencies': currencies
    }
    return render_to_response('crypto.html', result)


def write_header():
    with open(FILENAME, "a", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=COLUMNS)
        writer.writeheader()


def remove_old_results_file_if_exist():
    path = os.path.join("", FILENAME)
    try:
        os.remove(path)
    except FileNotFoundError:
        return


def filter_currencies_and_get_result(currencies):
    result = []
    data_for_data_frame = []
    for c in currencies:
        if c:
            data_for_data_frame.append(json.loads(str(c)))
    data_frame = pandas.DataFrame(data=data_for_data_frame)
    sorted_data = data_frame.sort_values("percent_changed", ascending=False)[:250]
    print(sorted_data[:7])
    sorted_data.to_csv(FILENAME)
    with open(FILENAME, "r", newline="") as file:
        reader = csv.DictReader(file)
        for row in reader:
            result.append(Currency(name=row["name"], percent_changed=row["percent_changed"], value=row["value"],
                                   percent_changed_more=row["percent_changed_more"]))
    return result


def get_filtered_by_name_currencies_from_server(url, correct_names):
    currencies = json.loads(requests.get(url).text).get("pairs")
    filtered_currencies = []
    for currency in currencies:
        splited_name = currency.split("_")
        if splited_name[len(splited_name) - 1] in correct_names:
            filtered_currencies.append(currency)
    return filtered_currencies


def transform_currencies_to_params(currencies_names, interval, price_min, price_max):
    parameters = []
    for currencies_name in currencies_names:
        parameters.append(
            ParametersDto(currencyName=currencies_name, interval=interval, price_min=price_min,
                          price_max=price_max))
    return parameters
