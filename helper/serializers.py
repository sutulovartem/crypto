from rest_framework import serializers

from .models import InformationByCurrency


class InformationByCyrrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = InformationByCurrency
        fields = '__all__'
