from __future__ import absolute_import, unicode_literals

import celery

app = celery.Celery()

@app.task()
def task_number_one():
    print("Hello")
