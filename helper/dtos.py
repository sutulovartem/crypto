import json


class Currency:
    def __init__(self, name, percent_changed, value, percent_changed_more):
        self.name = name
        self.percent_changed = percent_changed
        self.value = value
        self.percent_changed_more = percent_changed_more

    def __repr__(self) -> str:
        return json.dumps({'name': self.name, 'percent_changed': self.percent_changed, 'value': self.value,
                           'percent_changed_more': self.percent_changed_more})

    def __str__(self) -> str:
        return self.__repr__()


class ParametersDto:
    def __init__(self, currencyName, interval, price_min, price_max):
        super().__init__()
        self.currencyName = currencyName
        self.interval = interval
        self.price_min = price_min
        self.price_max = price_max
